-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Creato il: Mar 16, 2017 alle 19:11
-- Versione del server: 5.1.73
-- Versione PHP: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `letitwine_quizvinitaly2017`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie`
--

CREATE TABLE `categorie` (
  `id_cat` int(11) NOT NULL,
  `nome_cat` text NOT NULL,
  `img_titolo` text NOT NULL,
  `img_sfondo` text NOT NULL,
  `img_border` text NOT NULL,
  `img_box` text NOT NULL,
  `registrazione_cat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dump dei dati per la tabella `categorie`
--

INSERT INTO `categorie` (`id_cat`, `nome_cat`, `img_titolo`, `img_sfondo`, `img_border`, `img_box`, `registrazione_cat`) VALUES
(1, 'Wine&People', 'img/logo_cat/wine&people.png', 'img/sfondo_cat/wine&people.jpg', 'img/border_cat/wine&people.png', 'img/sfondo_box_cat/wine&people.png', '2017-03-13 13:40:21'),
(2, 'Wine itineraries', 'img/logo_cat/wineitineraries.png', 'img/sfondo_cat/wineitineraries.jpg', 'img/border_cat/wineitineraries.png', 'img/sfondo_box_cat/wineitineraries.png', '2017-03-13 16:29:46'),
(3, 'Style&Food', 'img/logo_cat/style_food.png', 'img/sfondo_cat/style&food.jpg', 'img/border_cat/style&food.png', 'img/sfondo_box_cat/style&food.png', '2017-03-13 16:29:46'),
(4, 'Tips', 'img/logo_cat/tips.png', 'img/sfondo_cat/tips.jpg', 'img/border_cat/tips.png', 'img/sfondo_box_cat/tips.png', '2017-03-13 16:29:46');

-- --------------------------------------------------------

--
-- Struttura della tabella `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('3q5ffomck6lmged8cm8eu85hn793iuno', '2a03:2880:3010:cfe9:face:b00c:0:8000', 1489686794, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393638363739343b),
('72hpkl5cu33mahbb88edl6h70vn7r222', '2a03:2880:3010:cff9:face:b00c:0:8000', 1489686813, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393638363831333b66625f313735363431383932383030373231335f73746174657c733a33323a226664306232363262303761363163363464643838323365616261663434333631223b),
('82204o4n5322at6ieil3p461cajssg6b', '194.79.193.150', 1489595217, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393539353231373b66625f313735363431383932383030373231335f73746174657c733a33323a226562343437613265336234393366396164396362633331363330623136303331223b),
('96ejlc2l07m6ndo1me2p14mq08j1gimf', '194.79.193.150', 1489687780, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393638373632303b),
('aqgknide5ac91rse62bl8m2m7v4q2drd', '194.79.193.150', 1489592202, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393539323139383b66625f313735363431383932383030373231335f73746174657c733a33323a226464333639663565626132383836623339616636663232303665643431386438223b),
('b5uf5hs24k488hhtetot0dvhah29c5bv', '194.79.193.150', 1489593797, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393539333739373b646f6d616e6461317c733a313a2231223b69645f646f6d616e6461317c733a313a2237223b646f6d616e6461327c733a313a2231223b69645f646f6d616e6461327c733a313a2232223b646f6d616e6461337c733a313a2231223b69645f646f6d616e6461337c733a323a223130223b646f6d616e6461347c733a313a2231223b69645f646f6d616e6461347c733a323a223132223b646f6d616e6461357c733a313a2231223b69645f646f6d616e6461357c733a323a223230223b646f6d616e6461367c733a313a2231223b69645f646f6d616e6461367c733a323a223139223b646f6d616e6461377c733a313a2230223b69645f646f6d616e6461377c733a323a223238223b646f6d616e6461387c733a313a2230223b69645f646f6d616e6461387c733a323a223237223b),
('flmh2dvdbbv5qnfm389jogsheltocnsh', '2a03:2880:3010:cff6:face:b00c:0:8000', 1489686805, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393638363830353b),
('hc53uj6bss6ppsop37hmp77foficlqlo', '2a03:2880:3010:cff7:face:b00c:0:8000', 1489687641, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393638373634313b),
('hf3avbtsclacpveqhtlvbvoo175a9r0f', '194.79.193.150', 1489593797, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393539333739373b646f6d616e6461317c733a313a2231223b69645f646f6d616e6461317c733a313a2237223b646f6d616e6461327c733a313a2231223b69645f646f6d616e6461327c733a313a2232223b646f6d616e6461337c733a313a2231223b69645f646f6d616e6461337c733a323a223130223b646f6d616e6461347c733a313a2231223b69645f646f6d616e6461347c733a323a223132223b646f6d616e6461357c733a313a2231223b69645f646f6d616e6461357c733a323a223230223b646f6d616e6461367c733a313a2231223b69645f646f6d616e6461367c733a323a223139223b646f6d616e6461377c733a313a2230223b69645f646f6d616e6461377c733a323a223238223b646f6d616e6461387c733a313a2230223b69645f646f6d616e6461387c733a323a223237223b),
('htmdnaj2piqlpjv05dtj0qjnjmp326du', '194.79.193.150', 1489591901, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393539313839363b646f6d616e6461317c733a313a2231223b69645f646f6d616e6461317c733a313a2235223b646f6d616e6461327c733a313a2231223b69645f646f6d616e6461327c733a313a2231223b),
('nkagbeas1d0mhbm4pri1j2p9le69on8o', '2a03:2880:3010:cfe9:face:b00c:0:8000', 1489687632, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393638373633323b),
('op9sv9hme2fda7jpbpl0p6f5dcuke900', '194.79.193.150', 1489686723, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393638363730373b646f6d616e6461317c733a313a2231223b69645f646f6d616e6461317c733a313a2235223b646f6d616e6461327c733a313a2230223b69645f646f6d616e6461327c733a313a2236223b646f6d616e6461337c733a313a2231223b69645f646f6d616e6461337c733a323a223135223b646f6d616e6461347c733a313a2231223b69645f646f6d616e6461347c733a323a223133223b646f6d616e6461357c733a313a2230223b69645f646f6d616e6461357c733a323a223231223b646f6d616e6461367c733a313a2231223b69645f646f6d616e6461367c733a323a223233223b646f6d616e6461377c733a313a2231223b69645f646f6d616e6461377c733a323a223330223b646f6d616e6461387c733a313a2230223b69645f646f6d616e6461387c733a323a223331223b),
('pdjgqgjacr0n7kdmf3g42d6lb9b0fohl', '2a03:2880:3010:cfe0:face:b00c:0:8000', 1489686773, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393638363737333b),
('rhqtec5vouqg2p36tk7nf807dv8hn919', '194.79.193.150', 1489595217, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393539353231373b),
('sa32cse056obo91u6ap82e5g8f0tp0h6', '194.79.193.150', 1489592412, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393539323138383b646f6d616e6461317c733a313a2230223b69645f646f6d616e6461317c733a313a2235223b646f6d616e6461327c733a313a2231223b69645f646f6d616e6461327c733a313a2232223b646f6d616e6461337c733a313a2230223b69645f646f6d616e6461337c733a323a223133223b646f6d616e6461347c733a313a2230223b69645f646f6d616e6461347c733a323a223135223b646f6d616e6461357c733a313a2230223b69645f646f6d616e6461357c733a323a223230223b646f6d616e6461367c733a313a2230223b69645f646f6d616e6461367c733a323a223231223b646f6d616e6461377c733a313a2231223b69645f646f6d616e6461377c733a323a223238223b646f6d616e6461387c733a313a2230223b69645f646f6d616e6461387c733a323a223330223b),
('sjth32n4nkk09emucgpp6dh1tv3sq5o8', '194.79.193.150', 1489686703, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393638363730333b646f6d616e6461317c733a313a2230223b69645f646f6d616e6461317c733a313a2234223b646f6d616e6461327c733a313a2230223b69645f646f6d616e6461327c733a313a2231223b646f6d616e6461337c733a313a2231223b69645f646f6d616e6461337c733a323a223131223b646f6d616e6461347c733a313a2231223b69645f646f6d616e6461347c733a323a223130223b646f6d616e6461357c733a313a2231223b69645f646f6d616e6461357c733a323a223233223b646f6d616e6461367c733a313a2230223b69645f646f6d616e6461367c733a323a223232223b646f6d616e6461377c733a313a2231223b69645f646f6d616e6461377c733a323a223236223b646f6d616e6461387c733a313a2231223b69645f646f6d616e6461387c733a323a223330223b),
('tvuef050f362nlp46l4d4obt5fj22la7', '194.79.193.150', 1489591890, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393539313839303b646f6d616e6461317c733a313a2230223b69645f646f6d616e6461317c733a313a2232223b646f6d616e6461327c733a313a2230223b69645f646f6d616e6461327c733a313a2233223b646f6d616e6461337c733a313a2231223b69645f646f6d616e6461337c733a323a223133223b646f6d616e6461347c733a313a2231223b69645f646f6d616e6461347c733a323a223131223b646f6d616e6461357c733a313a2230223b69645f646f6d616e6461357c733a323a223138223b646f6d616e6461367c733a313a2231223b69645f646f6d616e6461367c733a323a223234223b646f6d616e6461377c733a313a2231223b69645f646f6d616e6461377c733a323a223330223b646f6d616e6461387c733a313a2231223b69645f646f6d616e6461387c733a323a223239223b);

-- --------------------------------------------------------

--
-- Struttura della tabella `domande`
--

CREATE TABLE `domande` (
  `id_domanda` int(11) NOT NULL,
  `id_cat` int(11) NOT NULL,
  `nome_domanda` longtext NOT NULL,
  `link_articolo` longtext NOT NULL,
  `registrazione_domanda` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dump dei dati per la tabella `domande`
--

INSERT INTO `domande` (`id_domanda`, `id_cat`, `nome_domanda`, `link_articolo`, `registrazione_domanda`) VALUES
(1, 1, 'Chi è l’attore principale di The Duel of Wine, il film che vede protagonista la grande Italia del vino?', 'https://www.letitwine.com/the-duel-of-wine-dal-29-settembre-nelle-sale-italiane/', '2017-03-13 13:59:55'),
(2, 1, 'Qual è lo stato dove si consuma più vino al mondo?', 'https://www.letitwine.com/lo-si-consuma-piu-vino-al-mondo-vaticano/', '2017-03-13 13:59:55'),
(3, 1, 'Quale tra questi vini di Carpineto è entrato nella TOP 100 mondiale di Wine Spectator nel 2016?', 'https://www.letitwine.com/nobile-montepulciano-riserva-2011-carpineto-nella-top-100-wine-spectator/', '2017-03-13 13:59:55'),
(4, 1, 'Una start up spagnola ha recentemente inventato un vino dal colore particolare. Quale?', 'https://www.letitwine.com/la-rivoluzione-del-vino-blu/', '0000-00-00 00:00:00'),
(5, 1, 'A chi dobbiamo la nascita dell\'aperitivo?', 'https://www.letitwine.com/breve-storia-dellaperitivo/ ', '0000-00-00 00:00:00'),
(6, 1, 'Chi si è aggiudicato il titolo di migliore sommelier del mondo 2016?', 'https://www.letitwine.com/svedese-miglior-sommelier-al-mondo/ ', '0000-00-00 00:00:00'),
(7, 1, 'Quale giovane artista italiana ha scritto \"Autoctono italiano\", canzone dedicata al vino e che traduce in note 70 vitigni di ogni regione d\'Italia?', 'https://www.letitwine.com/pilar-la-mia-canzone-dedicata-al-vino/', '0000-00-00 00:00:00'),
(8, 1, 'Quale tra questi ottimi vini si è guadagnato le \"Super Tre Stelle\" della Guida Oro I Vini di Veronelli 2017?', 'https://www.letitwine.com/farnito-cabernet-sauvignon-2011-guadagna-le-super-tre-stelle-2017-veronelli/', '0000-00-00 00:00:00'),
(9, 2, 'In quale di questi stati si produce l\'Eiswein?', 'https://www.letitwine.com/welcome-to-toronto/', '0000-00-00 00:00:00'),
(10, 2, 'In quale regione italiana è possibile bere il vino Nobile di Montepulciano e il Morellino di Scansano?', 'https://www.letitwine.com/toscana-terra-grandi-vini/', '0000-00-00 00:00:00'),
(11, 2, 'La celeberrima Route 62, ossia la più lunga strada del vino al mondo. Dove si trova?', 'https://www.letitwine.com/sud-africa-le-strade-del-vino-non-ti-aspetti/', '0000-00-00 00:00:00'),
(12, 2, 'Per visitare la \"Cité du Vin\", il parco divertimenti dedicato al vino, dove devi andare?', 'https://www.letitwine.com/la-cite-du-vin-bordeaux-un-parco-tema-dedicato-al-vino/ ', '0000-00-00 00:00:00'),
(13, 2, 'Qual è il vitigno maggiormente coltivato nella Napa Valley?', 'https://www.letitwine.com/napa-valley-il-cuore-della-viticoltura-californiana/ ', '0000-00-00 00:00:00'),
(14, 2, 'Cittadina italiana nota per la produzione di uno dei vini più famosi al mondo e per le sue bellezze culturali, tra cui la statua di Giovanni da Verrazzano, scopritore della baia di New York. Come si chiama?', 'https://www.letitwine.com/alla-scoperta-della-porta-del-chianti/ ', '0000-00-00 00:00:00'),
(15, 2, 'Bianco, secco e leggermente frizzante. È apprezzato per il suo basso tasso alcolico e la scarsa acidità. Quale nazione devi visitare per assaggiare il tipico Vinho Verde?', 'https://www.letitwine.com/portogallo-non-solo-porto-madeira/ ', '0000-00-00 00:00:00'),
(16, 2, 'Caponata di Melanzane e Cerasuolo di Vittoria: quale regione italiana dovresti visitare per gustare al meglio quest\'ottimo abbinamento?', 'https://www.letitwine.com/la-top-5-dei-piatti-tipici-regionali-abbinamento-vino/', '0000-00-00 00:00:00'),
(17, 3, 'Una casa dolciaria americana ha recentemente lanciato sul mercato una linea di prodotti al gusto di Cabernet Sauvignon, Chardonnay e Merlot. Di cosa si tratta?', 'https://www.letitwine.com/lecca-lecca-al-vino-rosso/', '0000-00-00 00:00:00'),
(18, 3, 'Sei ad una cena elegante e un ospite ti chiede gentilmente del vino. Come lo servirai se vuoi fare bella figura?', 'https://www.letitwine.com/il-galateo-del-vino/ ', '0000-00-00 00:00:00'),
(19, 3, 'Le verdure sono caratterizzate da un sapore tendenzialmente dolce che ben si abbina con:', 'https://www.letitwine.com/come-abbinare-il-vino-nella-dieta-vegetariana/', '0000-00-00 00:00:00'),
(20, 3, 'Con quali vini si abbina perfettamente la pizza ai quattro formaggi?', 'https://www.letitwine.com/abbinare-pizza-e-vino-si-puo/ ', '0000-00-00 00:00:00'),
(21, 3, 'Secondo l\'Associazione Italiana Sommelier, a quali formaggi si abbinano i vini rossi di grande struttura ed invecchiati, dal profumo abbastanza intenso, molto equilibrati, sapidi, giustamente tannici e caldi?', 'https://www.letitwine.com/formaggio-e-vino-un-matrimonio-damore/', '0000-00-00 00:00:00'),
(22, 3, 'Quale gustoso alimento è difficile, se non impossibile, da abbinare al vino a causa della \"cinarina\" da esso contenuta?', 'https://www.letitwine.com/combinazioni-imbarazzanti-i-cibi-che-non-gradiscono-il-vino/ ', '0000-00-00 00:00:00'),
(23, 3, 'A quale vino si abbinano tradizionalmente le castagne?', 'https://www.letitwine.com/novembre-tempo-vino-novello-gli-abbinamenti-stagione/ ', '0000-00-00 00:00:00'),
(24, 3, 'Insieme a quale dessert tipico toscano viene solitamente servito il Vin Santo?', 'https://www.letitwine.com/vini-dolci-italiani-imparare-a-conoscerli/ ', '0000-00-00 00:00:00'),
(25, 4, 'Qual è il giusto calice per servire rossi invecchiati e molto importanti?', 'https://www.letitwine.com/quello-ce-sapere-sui-bicchieri-vino/ ', '0000-00-00 00:00:00'),
(26, 4, 'Per conservare correttamente una bottiglia di vino in casa, la temperatura ideale dovrà:\r\n', 'https://www.letitwine.com/conservare-vino-casa-consigli-evitare-spiacevoli-sorprese/ ', '2017-03-14 10:41:26'),
(27, 4, 'Perché il vino a volte “sa di tappo”?\r\n', 'https://www.letitwine.com/sa-di-tappo/  ', '2017-03-14 10:41:26'),
(28, 4, 'A quali sostanze naturali presenti nel vino si devono le sue proprietà antinfiammatorie, antiossidanti e cardioprotettive?', 'https://www.letitwine.com/cosa-sono-i-polifenoli/', '2017-03-14 10:41:26'),
(29, 4, 'Quale aroma è tipico dei vini che vengono maturati in botti di legno?\r\n', 'https://www.letitwine.com/quello-ce-sapere-sulle-botti-vino', '2017-03-14 10:41:26'),
(30, 4, 'Quando un vino ha un colore tendente al marrone rivela:\r\n\r\n', 'https://www.letitwine.com/occhio-ai-difetti-riconoscere-un-buon-vino/ ', '2017-03-14 10:41:26'),
(31, 4, 'A cosa serve il decanter?\r\n\r\n', 'https://www.letitwine.com/cose-e-a-cosa-serve-il-decanter/ ', '2017-03-14 10:41:26');

-- --------------------------------------------------------

--
-- Struttura della tabella `risposte`
--

CREATE TABLE `risposte` (
  `id_risposta` int(11) NOT NULL,
  `id_domanda` int(11) NOT NULL,
  `id_cat` int(11) NOT NULL,
  `nome_risposta` text NOT NULL,
  `check_esatta` int(11) NOT NULL,
  `resgistrazione_risposta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `risposte`
--

INSERT INTO `risposte` (`id_risposta`, `id_domanda`, `id_cat`, `nome_risposta`, `check_esatta`, `resgistrazione_risposta`) VALUES
(1, 1, 1, 'Keanu Reeves', 1, '2017-03-13 14:01:22'),
(2, 1, 1, 'Charlie Arturaola', 0, '2017-03-13 14:01:22'),
(3, 1, 1, 'Joe Bastianich ', 1, '2017-03-13 14:01:22'),
(4, 2, 1, 'Francia', 1, '2017-03-13 16:27:15'),
(5, 2, 1, 'Spagna', 1, '2017-03-13 16:27:15'),
(6, 2, 1, 'Vaticano', 0, '2017-03-13 16:27:25'),
(7, 3, 1, 'Dogajolo Toscano Rosso', 1, '2017-03-13 16:28:12'),
(8, 3, 1, 'Nobile di Montepulciano Riserva 2011', 0, '2017-03-13 16:28:12'),
(9, 3, 1, 'Chianti Classico 2013', 1, '2017-03-13 16:28:26'),
(10, 4, 1, 'Arancione\r\n', 1, '2017-03-13 17:28:24'),
(11, 4, 1, 'Violetto\r\n', 1, '2017-03-13 17:28:24'),
(12, 4, 1, 'Blu elettrico\r\n', 0, '2017-03-13 17:28:24'),
(13, 5, 1, 'Ippocrate', 0, '2017-03-13 17:28:24'),
(14, 5, 1, 'Isaac Newton', 1, '2017-03-13 17:28:24'),
(15, 5, 1, 'Giorgia', 1, '2017-03-13 17:28:24'),
(16, 6, 1, 'L’italiano Luca Gardini', 1, '2017-03-13 17:30:21'),
(17, 6, 1, 'Lo svedese Jon Arvid Rosengren', 0, '2017-03-13 17:30:21'),
(18, 6, 1, 'Il francese David Biraud', 1, '2017-03-13 17:30:21'),
(19, 7, 1, 'Emma\r\n', 1, '2017-03-13 17:30:21'),
(20, 7, 1, 'Nina Zilli', 1, '2017-03-13 17:30:21'),
(21, 7, 1, 'Pilar', 0, '2017-03-13 17:30:21'),
(22, 8, 1, 'Farnito Cabernet Sauvignon 2011\r\n', 0, '2017-03-13 17:30:21'),
(23, 8, 1, '“Valcolomba” Merlot della Maremma Toscana 2011', 1, '2017-03-13 17:30:21'),
(24, 8, 1, 'Appodiato di Chianciano “La Fornace” 2007\r\n', 1, '2017-03-13 17:30:21'),
(25, 9, 2, 'Sud Africa\r\n', 1, '2017-03-13 17:30:21'),
(26, 9, 2, 'Canada', 0, '2017-03-13 17:30:21'),
(27, 9, 2, 'Spagna\r\n', 1, '2017-03-13 17:30:21'),
(28, 10, 2, 'Piemonte', 1, '2017-03-13 17:30:21'),
(29, 10, 2, 'Marche', 1, '2017-03-13 17:30:21'),
(30, 10, 2, 'Toscana', 0, '2017-03-13 17:30:21'),
(31, 11, 2, 'Sud Africa', 0, '2017-03-13 17:30:21'),
(32, 11, 2, 'California', 1, '2017-03-13 17:30:21'),
(33, 11, 2, 'Australia', 1, '2017-03-13 17:30:21'),
(34, 12, 2, 'Québec, Canada\r\n', 1, '2017-03-13 17:30:21'),
(35, 12, 2, 'Bordeaux, Francia\r\n', 0, '2017-03-13 17:30:21'),
(36, 12, 2, 'Bruxelles, Belgio', 1, '2017-03-13 17:30:21'),
(37, 13, 2, 'Sangiovese\r\n', 1, '2017-03-13 17:30:21'),
(38, 13, 2, 'Barbera', 1, '2017-03-13 17:30:21'),
(39, 13, 2, 'Cabernet Sauvignon\r\n', 0, '2017-03-13 17:30:21'),
(40, 14, 2, 'Greve in Chianti\r\n', 0, '2017-03-13 17:30:21'),
(41, 14, 2, 'Barolo\r\n', 1, '2017-03-13 17:30:21'),
(42, 14, 2, 'Bolgheri', 1, '2017-03-13 17:30:21'),
(43, 15, 2, 'Brasile', 1, '2017-03-13 17:30:21'),
(44, 15, 2, 'Portogallo', 0, '2017-03-13 17:30:21'),
(45, 15, 2, 'Spagna', 1, '2017-03-13 17:30:21'),
(46, 16, 2, 'Abruzzo', 1, '2017-03-13 17:30:21'),
(47, 16, 2, 'Calabria', 1, '2017-03-13 17:30:21'),
(48, 16, 2, 'Sicilia', 0, '2017-03-13 17:30:21'),
(49, 17, 3, 'Lecca Lecca', 0, '2017-03-13 17:30:21'),
(50, 17, 3, 'Chewing gum', 1, '2017-03-13 17:30:21'),
(51, 17, 3, 'Barrette di riso soffiato', 1, '2017-03-13 17:30:21'),
(52, 18, 3, 'Versando dalla sua sinistra', 1, '2017-03-13 17:30:21'),
(53, 18, 3, 'Versando dalla sua destra', 0, '2017-03-13 17:30:21'),
(54, 18, 3, 'Gli porgerai la bottiglia', 1, '2017-03-13 17:30:21'),
(55, 19, 3, 'Rossi corposi, intensi e di grande struttura', 1, '2017-03-13 17:30:21'),
(56, 19, 3, 'Bianchi e rosati, freschi e di buona acidità\r\n', 0, '2017-03-13 17:30:21'),
(57, 19, 3, 'Non si abbinano ad alcun vino\r\n', 1, '2017-03-13 17:30:21'),
(58, 20, 3, 'Rossi corposi, intensi e di grande struttura', 1, '2017-03-13 17:30:21'),
(59, 20, 3, 'Bianchi giovani e freschi', 1, '2017-03-13 17:30:21'),
(60, 20, 3, 'Spumanti brut', 0, '2017-03-13 17:30:21'),
(61, 21, 3, 'Formaggi a pasta dura cotta, molto stagionati', 0, '2017-03-13 17:30:21'),
(62, 21, 3, 'Formaggi freschi non salati', 1, '2017-03-13 17:30:21'),
(63, 21, 3, 'Formaggi a pasta dura non cotta', 1, '2017-03-13 17:30:21'),
(64, 22, 3, 'Fagioli', 1, '2017-03-13 17:30:21'),
(65, 22, 3, 'Carciofo', 0, '2017-03-13 17:30:21'),
(66, 22, 3, 'Tartufo', 1, '2017-03-13 17:30:21'),
(67, 23, 3, 'Bianco giovane\r\n', 1, '2017-03-13 17:30:21'),
(68, 23, 3, 'Rosso invecchiato\r\n', 1, '2017-03-13 17:30:21'),
(69, 23, 3, 'Novello\r\n\r\n', 0, '2017-03-13 17:30:21'),
(70, 24, 3, 'Cantucci\r\n\r\n', 0, '2017-03-13 17:30:21'),
(71, 24, 3, 'Panforte\r\n\r\n', 1, '2017-03-13 17:30:21'),
(72, 24, 3, 'Panpepato\r\n\r\n', 1, '2017-03-13 17:30:21'),
(73, 25, 4, 'Il Tulipano\r\n', 1, '2017-03-13 17:30:21'),
(74, 25, 4, 'Il Ballon\r\n', 0, '2017-03-13 17:30:21'),
(75, 25, 4, 'Il Flute\r\n', 1, '2017-03-13 17:30:21'),
(76, 26, 4, 'Essere intorno ai 22°C\r\n', 1, '2017-03-13 17:30:21'),
(77, 26, 4, 'Non superare i 10°C \r\n', 1, '2017-03-13 17:30:21'),
(78, 26, 4, 'Non superare i 16°C e non scendere oltre i 10\r\n', 0, '2017-03-13 17:30:21'),
(79, 27, 4, 'Per via di un fungo che si sviluppa nel sughero\r\n', 0, '2017-03-13 17:30:21'),
(80, 27, 4, 'A causa della scarsa ossigenazione', 1, '2017-03-13 17:30:21'),
(81, 27, 4, 'Perché il vino è stato conservato a contatto col tappo', 1, '2017-03-13 17:30:21'),
(82, 28, 4, 'Purine', 1, '2017-03-13 17:30:21'),
(83, 28, 4, 'Polifenoli', 0, '2017-03-13 17:30:21'),
(84, 28, 4, 'Omega 3 e 6', 1, '2017-03-13 17:30:21'),
(85, 29, 4, 'Sottobosco\r\n', 1, '2017-03-13 17:30:21'),
(86, 29, 4, 'Floreale\r\n', 1, '2017-03-13 17:30:21'),
(87, 29, 4, 'Vaniglia\r\n', 0, '2017-03-13 17:30:21'),
(88, 30, 4, 'Che ha sorpassato il limite di bevibilità\r\n', 0, '2017-03-13 17:30:21'),
(89, 30, 4, 'Che è una Riserva di pregio\r\n\r\n', 1, '2017-03-13 17:30:21'),
(90, 30, 4, 'Che si tratta di un vino che ha ancora bisogno di maturare\r\n', 1, '2017-03-13 17:30:21'),
(91, 31, 4, 'Conservare il vino \r\n\r\n', 1, '2017-03-13 17:30:21'),
(92, 31, 4, 'Separare il vino da eventuali sedimenti ed ossigenarlo\r\n\r\n', 0, '2017-03-13 17:30:21'),
(93, 31, 4, 'Mantenere intatto il frizzante dei vini durante il servizio\r\n\r\n', 1, '2017-03-13 17:30:21');

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `oauth_provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `oauth_uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `picture_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `oauth_provider`, `oauth_uid`, `first_name`, `last_name`, `email`, `gender`, `locale`, `picture_url`, `profile_url`, `created`, `modified`) VALUES
(1, 'facebook', '412068685812490', 'Simone', 'Felici', 'felici.simone@gmail.com', 'male', 'it_IT', 'https://scontent.xx.fbcdn.net/v/t1.0-1/c0.8.50.50/p50x50/12112280_153423078343720_1858906614677925621_n.jpg?oh=bd5ac2491ddb87e5470f6eb0dd3aae1e&oe=5927D6D9', 'https://www.facebook.com/412068685812490', '2017-03-15 15:20:42', '2017-03-15 15:20:42'),
(2, 'facebook', '10209859112729434', 'Luisa', 'Miolano', 'luisa.miolano@gmail.com', 'female', 'it_IT', 'https://scontent.xx.fbcdn.net/v/t1.0-1/c0.5.50.50/p50x50/15747858_10209180312159844_7613057802053205636_n.jpg?oh=83314160aefe06b2b99e6da33df31f72&oe=5969E2E0', 'https://www.facebook.com/10209859112729434', '2017-03-15 15:23:49', '2017-03-16 17:51:47'),
(3, 'facebook', '10208762563409949', 'Gennaro', 'Esposito', 'gegennaro102@gmail.com', 'male', 'it_IT', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/11817186_10204735080965405_6472893059586625710_n.jpg?oh=ec37b018869614256291a48d1aafdef9&oe=596E71DE', 'https://www.facebook.com/10208762563409949', '2017-03-15 15:28:32', '2017-03-15 15:31:44'),
(4, 'facebook', '10155162579359228', 'Federica', 'De Col', 'federicadecol@gmail.com', 'female', 'it_IT', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/13434869_10154289074289228_3986933503114025533_n.jpg?oh=241e3475255d4dd588a196a8450fcf1a&oe=592C2B3C', 'https://www.facebook.com/10155162579359228', '2017-03-15 15:36:28', '2017-03-15 15:36:28');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id_cat`);

--
-- Indici per le tabelle `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indici per le tabelle `domande`
--
ALTER TABLE `domande`
  ADD PRIMARY KEY (`id_domanda`);

--
-- Indici per le tabelle `risposte`
--
ALTER TABLE `risposte`
  ADD PRIMARY KEY (`id_risposta`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id_cat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT per la tabella `domande`
--
ALTER TABLE `domande`
  MODIFY `id_domanda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT per la tabella `risposte`
--
ALTER TABLE `risposte`
  MODIFY `id_risposta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
