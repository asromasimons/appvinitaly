<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gestionerisposte extends CI_Model{



    public function get_risposte_domanda($domanda){

        $this->db->select('*')
            ->from('risposte')
            ->where('id_domanda', $domanda)
            ->limit(3);
           return $this->db->get();

    }

    public function get_esito_risposta($id_risposta){

        $this->db->select('check_esatta')
            ->from('risposte')
            ->where('id_risposta',$id_risposta);
        $query = $this->db->get();
        $result = $query->row();
        if ($query->result()) {
            return $result->check_esatta;
        } else{
            return false;
        }

    }








}
