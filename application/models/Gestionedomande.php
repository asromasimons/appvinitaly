<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gestionedomande extends CI_Model{



    public function get_domanda_categoria($id_cat){

        $this->db->select('*')
            ->from('categorie')
            ->join('domande', 'categorie.id_cat=domande.id_cat', 'inner')
            ->where('categorie.id_cat',$id_cat)
            ->limit(1)
            ->order_by('rand()');
           return $this->db->get();

    }

    public function get_domanda_categoria2($id_cat,$id_domanda){

        $this->db->select('*')
            ->from('categorie')
            ->join('domande', 'categorie.id_cat=domande.id_cat', 'inner')
            ->where('categorie.id_cat',$id_cat)
            ->where('domande.id_domanda!=',$id_domanda)
            ->limit(1)
            ->order_by('rand()');
        return $this->db->get();

    }

    public function get_domanda($id_domanda){

        $this->db->select('nome_domanda')
            ->from('domande')
            ->where('id_domanda',$id_domanda);
        $query = $this->db->get();
        $result = $query->row();
        if ($query->result()) {
            return $result->nome_domanda;
        } else{
            return false;
        }

    }

    public function get_url_domanda($id_domanda){

        $this->db->select('link_articolo')
            ->from('domande')
            ->where('id_domanda',$id_domanda);
        $query = $this->db->get();
        $result = $query->row();
        if ($query->result()) {
            return $result->link_articolo;
        } else{
            return false;
        }

    }






}
