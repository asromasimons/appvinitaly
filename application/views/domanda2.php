<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<?php $this->load->view('layout/header'); ?>
    <style>
        .button_home:hover{
            color:#009788;
        }
    </style>

<!-- Header -->
<header id="top" class="header" style="background-image:url('<?php echo base_url().$domande_cat['img_sfondo'];?>')">


    <div class="container_alto">
        <img class="imglogoalto" src="<?php echo base_url();?>img/loghi/tell_me_wine_chiaro.png">
    </div>



    <div class="container"  style="-webkit-border-image: url(<?php echo base_url().$domande_cat['img_border'];?>) 30 round;
    -o-border-image: url(<?php echo base_url().$domande_cat['img_border'];?>) 30 round;
    border-image: url(<?php echo base_url().$domande_cat['img_border'];?>) 30 round;">

        <div class="border-interno" style="background-image:url('<?php echo base_url().$domande_cat['img_box'];?>')">
        <img class="imgtitolobox" src="<?php echo base_url(). $domande_cat['img_titolo'];?>">

        <p class="text-box" style="margin-bottom:20px;">
          <span style="color:#ffffff;">
          <?php

          echo $domande_cat['nome_domanda'];

          ?>
</span>
        </p>


            <form method="POST" action="<?php echo base_url();?>start/gioco_quiz_wine_itineraries/<?php echo $domande_cat['id_domanda'];?>">
                <?php foreach($risposte_domande as $risposte){?>

                <button class="button_home"  name="risposta" value="<?php echo $risposte['id_risposta'];?>" style="width:90%;margin-top:5px;"><?php echo $risposte['nome_risposta'];?></button>
                    <?php } ?>
            </form>



        </div>
    </div>



    <div class="container_basso" >
        <img class="imglogobasso" src="<?php echo base_url();?>img/loghi/letitwine_bianco.png">
    </div>

</header>


<?php $this->load->view('layout/footer'); ?>