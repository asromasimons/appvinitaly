<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Quiz Vinitaly | Livello astemio</title>
        <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="<?php echo base_url();?>css/stylish-portfolio.css" rel="stylesheet">
        <link href="<?php echo base_url();?>css/custom.css" rel="stylesheet">

        <meta property="og:url"           content="https://www.letitwine.com/quizvinitaly2017/astemio" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="Letitwine | Quiz Vinitaly 2017 - Astemio" />
        <meta property="og:description"   content="Your description" />
        <meta property="og:image"         content="https://www.letitwine.com/quizvinitaly2017/img/box_ris/astemio.png" />


    </head>
<body>


<div class="sfondorisultato" style=" background-image: url('<?php echo base_url();?>img/sfondo_ris/astemio.jpg');">

    <div class="container_ris">
        <div class="sfondobox_dentro" style="background-image: url('<?php echo base_url();?>img/box_ris/astemio.png');">
         <div>
        <img src="<?php echo base_url();?>img/titolo_ris/astemio.png" style="border-bottom:2px solid #ffffff;max-width:100%;">
        </div>
        <div class="risultatobox">


            <span class="titoloris">LIVELLO: ASTEMIO</span>
        </div>
        <div class="testoris">
            Ti piacerà anche bere, ma al massimo sai riconoscere la differenza tra un vino pregiato e quello comprato al discount. Se vuoi davvero scoprire i piaceri che il vino ti potrà regalare, è meglio che ti impegni di più. Tanto.
        </div>
        </div>

    </div>

    <div class="box_social">
        <div class="col-md-6" style="padding:0px;float:left;width:50%;"><a href="https://www.facebook.com/sharer/sharer.php?u=https%3A//www.letitwine.com/quizvinitaly2017/astemio"><img src="<?php echo base_url();?>img/fb.png" style="max-width:100%;"></a></div>
        <div class="col-md-6" style="padding:0px;float:left;width:50%;"><a href="https://twitter.com/intent/tweet?text=https%3A//www.letitwine.com/quizvinitaly2017/astemio%20E tu che vino sei?%20-%20Sono uno Spumante%20"><img src="<?php echo base_url();?>img/tw.png" style="max-width:100%;"></a></div>

    </div>


    <div class="box_tell_me">
        <h3>AL VINITALY DIVENTA UN VIDEO BLOGGER CON</h3>
        <img src="<?php echo base_url();?>img/loghi/logoris.png" style="max-width:300px;">
        <p>
            Dal <span style="color:#24145e">9 al 12 aprile Let It Wine sarà al Vinitaly di Verona con Tell Me Wine.</span>
            Avrai la possibilità di realizzare un video assieme alla nostra troupe, per essere protagonista e diventare un vero e proprio video blogger del mondo del vino!
            Un modo originale e divertente per dare un volto alle notizie…e sarai tu a crearle!
            I video verranno pubblicati sul nostro wall e inseriti nel palinsesto editoriale di Let It Wine.
            <br /><span style="color:#24145e">Non mancare!</span>

        </p>

        <div class="col-md-4"><a href="https://www.letitwine.com/tellmewine-vinitaly-2017" target="_blank"><button class="scopridipiu">Scopri di più su tell me wine</button></a></div>
        <div class="col-md-4"><a href="<?php echo base_url();?>start/risposteesatte/astemio"><button class="scopririsposte">Scopri le risposte esatte</button></a></div>
        <div class="col-md-4"><a href="<?php echo base_url();?>start/"><button class="rifaiquiz">Rifai il quiz</button></a></div>
        <div style="clear:both;"></div>


    </div>



</div>



<!-- jQuery -->
<script src="<?php echo base_url();?>js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>


</body>

</html>