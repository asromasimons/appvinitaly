<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>



<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Quiz Vinitaly | Livello astemio</title>
        <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="<?php echo base_url();?>css/stylish-portfolio.css" rel="stylesheet">
        <link href="<?php echo base_url();?>css/custom.css" rel="stylesheet">


    </head>
<body>


<div class="sfondorisultato" style=" background-image: url('<?php echo base_url();?>img/sfondo_risposte.jpg');margin:auto;text-align:center;">

<img src="<?php echo base_url();?>img/loghi/tell_me_wine_chiaro.png" class="imglogoalto">
<h2 style="font-weight:400;">Hai risposto correttamente a <?php echo $numero_esatte;?> domande su 8</h2>
<h2>Leggi le soluzioni alle domande del quiz</h2>
    <img src="<?php echo base_url();?>img/loghi/let_titolo.png" style="max-width:350px;">

    <?php

    $urldomanda1=$this->gestionedomande->get_url_domanda($domande['domanda1']);
    $domande['domanda1']=$this->gestionedomande->get_domanda($domande['domanda1']);
    $domande['domanda1']=substr($domande['domanda1'],0,45);
    $urldomanda2=$this->gestionedomande->get_url_domanda($domande['domanda2']);
    $domande['domanda2']=$this->gestionedomande->get_domanda($domande['domanda2']);
    $domande['domanda2']=substr($domande['domanda2'],0,45);
    $urldomanda3=$this->gestionedomande->get_url_domanda($domande['domanda3']);
    $domande['domanda3']=$this->gestionedomande->get_domanda($domande['domanda3']);
    $domande['domanda3']=substr($domande['domanda3'],0,45);
    $urldomanda4=$this->gestionedomande->get_url_domanda($domande['domanda4']);
    $domande['domanda4']=$this->gestionedomande->get_domanda($domande['domanda4']);
    $domande['domanda4']=substr($domande['domanda4'],0,45);
    $urldomanda5=$this->gestionedomande->get_url_domanda($domande['domanda5']);
    $domande['domanda5']=$this->gestionedomande->get_domanda($domande['domanda5']);
    $domande['domanda5']=substr($domande['domanda5'],0,45);
    $urldomanda6=$this->gestionedomande->get_url_domanda($domande['domanda6']);
    $domande['domanda6']=$this->gestionedomande->get_domanda($domande['domanda6']);
    $domande['domanda6']=substr($domande['domanda6'],0,45);
    $urldomanda7=$this->gestionedomande->get_url_domanda($domande['domanda7']);
    $domande['domanda7']=$this->gestionedomande->get_domanda($domande['domanda7']);
    $domande['domanda7']=substr($domande['domanda7'],0,45);
    $urldomanda8=$this->gestionedomande->get_url_domanda($domande['domanda8']);
    $domande['domanda8']=$this->gestionedomande->get_domanda($domande['domanda8']);
    $domande['domanda8']=substr($domande['domanda8'],0,45);



    ?>


    <div style="margin-bottom:20px;">
        <div class="domandascura">

            <?php if($risposte['esito1']=='1'){ ?>
                <img src="<?php echo base_url();?>img/x.png" style="max-width:20px;">
            <?php }else{ ?>
                <img src="<?php echo base_url();?>img/v.png" style="max-width:20px;">
            <?php } ?>

            1. <?php echo $domande['domanda1'];?>... <a class="linkchiaro" href="<?php echo $urldomanda1;?>" target="_blank"><span style="text-transform:uppercase;font-weight:700;">Scopri la risposta</span></a></div>


        <div class="domandachiara">
            <?php if($risposte['esito2']=='1'){ ?>
                <img src="<?php echo base_url();?>img/x.png" style="max-width:20px;">
            <?php }else{ ?>
                <img src="<?php echo base_url();?>img/v.png" style="max-width:20px;">
            <?php } ?>

            2. <?php echo $domande['domanda2'];?>...  <a class="linkscuro" href="<?php echo $urldomanda2;?>" target="_blank"><span style="text-transform:uppercase;font-weight:700;">Scopri la risposta</span></a></div>

    <div class="domandascura">
        <?php if($risposte['esito3']=='1'){ ?>
            <img src="<?php echo base_url();?>img/x.png" style="max-width:20px;">
        <?php }else{ ?>
            <img src="<?php echo base_url();?>img/v.png" style="max-width:20px;">
        <?php } ?>

        3. <?php echo $domande['domanda3'];?>...  <a class="linkchiaro" href="<?php echo $urldomanda3;?>" target="_blank"><span style="text-transform:uppercase;font-weight:700;">Scopri la risposta</span></a></div>

        <div class="domandachiara">
            <?php if($risposte['esito4']=='1'){ ?>
                <img src="<?php echo base_url();?>img/x.png" style="max-width:20px;">
            <?php }else{ ?>
                <img src="<?php echo base_url();?>img/v.png" style="max-width:20px;">
            <?php } ?>

            4. <?php echo $domande['domanda4'];?>...  <a class="linkscuro" href="<?php echo $urldomanda4;?>" target="_blank"><span style="text-transform:uppercase;font-weight:700;">Scopri la risposta</span></a></div>


    <div class="domandascura">

        <?php if($risposte['esito5']=='1'){ ?>
            <img src="<?php echo base_url();?>img/x.png" style="max-width:20px;">
        <?php }else{ ?>
            <img src="<?php echo base_url();?>img/v.png" style="max-width:20px;">
        <?php } ?>

        5. <?php echo $domande['domanda5'];?>...  <a class="linkchiaro" href="<?php echo $urldomanda5;?>" target="_blank"><span style="text-transform:uppercase;font-weight:700;">Scopri la risposta</span></a></div>

        <div class="domandachiara">
            <?php if($risposte['esito6']=='1'){ ?>
                <img src="<?php echo base_url();?>img/x.png" style="max-width:20px;">
            <?php }else{ ?>
                <img src="<?php echo base_url();?>img/v.png" style="max-width:20px;">
            <?php } ?>


            6. <?php echo $domande['domanda6'];?>...  <a class="linkscuro" href="<?php echo $urldomanda6;?>" target="_blank"><span style="text-transform:uppercase;font-weight:700;">Scopri la risposta</span></a></div>


    <div class="domandascura">
        <?php if($risposte['esito7']=='1'){ ?>
            <img src="<?php echo base_url();?>img/x.png" style="max-width:20px;">
        <?php }else{ ?>
            <img src="<?php echo base_url();?>img/v.png" style="max-width:20px;">
        <?php } ?>

        7. <?php echo $domande['domanda7'];?>...  <a class="linkchiaro" href="<?php echo $urldomanda7;?>" target="_blank"><span style="text-transform:uppercase;font-weight:700;">Scopri la risposta</span></a></div>


        <div class="domandachiara">

            <?php if($risposte['esito8']=='1'){ ?>
                <img src="<?php echo base_url();?>img/x.png" style="max-width:20px;">
            <?php }else{ ?>
                <img src="<?php echo base_url();?>img/v.png" style="max-width:20px;">
            <?php } ?>

            8. <?php echo $domande['domanda8'];?>...  <a class="linkscuro" href="<?php echo $urldomanda8;?>" target="_blank"><span style="text-transform:uppercase;font-weight:700;">Scopri la risposta</span></a></div>

    </div>


    <div class="button_risultato">
    <a href="<?php echo base_url(). $url_ris;?>"><button class="scopridipiu">Torna al risultato</button></a>
    </div>


    <img src="<?php echo base_url();?>img/loghi/letitwine_bianco.png" class="imglogobasso">


</div>



<!-- jQuery -->
<script src="<?php echo base_url();?>js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>


</body>

</html