<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<?php $this->load->view('layout/header'); ?>


<!-- Header -->
<header id="top" class="header">


    <div class="container_alto">
        <img class="imglogoalto" src="<?php echo base_url();?>img/loghi/tell-me-wine-bianco.png">
    </div>



    <div class="container">

        <div class="border-interno">
        <img class="imgtitolobox" src="<?php echo base_url();?>img/titolo_home.png">

        <p class="text-box">
            Non manca mai sulla tua tavola, ti accompagna nelle occasioni più <br />importanti della tua vita ed è sempre

            presente nelle serate tra amici!  Il vino è chiaramente la tua passione, ma…quanto conosci davvero questa<br />

            meravigliosa bevanda e il mondo che le ruota attorno?
        </p>


           <?php  if(!empty($authUrl)) { ?>
            <a href="<?php echo $authUrl;?>"><button class="button_home">Mettiti alla prova con il quiz di let it wine!</button></a>
<?php } ?>

        </div>
    </div>



    <div class="container_basso" >
        <img class="imglogobasso" src="<?php echo base_url();?>img/loghi/letitwine_bianco.png">
    </div>

</header>


<?php $this->load->view('layout/footer'); ?>