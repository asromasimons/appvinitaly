<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Start extends CI_Controller {

    protected $view;

    function __construct() {
        parent::__construct();

        $this->load->model('user');
    }

	public function index()
	{
        include_once APPPATH."libraries/facebook-api-php-codexworld/facebook.php";

        // Facebook API Configuration
        $appId = '1756418928007213';
        $appSecret = '041972df58adfb7179aac44c35cfecb7';
        $fbPermissions = 'email';

        //Call Facebook API
        $facebook = new Facebook(array(
            'appId'  => $appId,
            'secret' => $appSecret

        ));
        $fbuser = $facebook->getUser();



        if ($fbuser) {
            $userProfile = $facebook->api('/me?fields=id,first_name,last_name,email,gender,locale,picture');
            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid'] = $userProfile['id'];
            $userData['first_name'] = $userProfile['first_name'];
            $userData['last_name'] = $userProfile['last_name'];
            $userData['email'] = $userProfile['email'];
            $userData['gender'] = $userProfile['gender'];
            $userData['locale'] = $userProfile['locale'];
            $userData['profile_url'] = 'https://www.facebook.com/'.$userProfile['id'];
            $userData['picture_url'] = $userProfile['picture']['data']['url'];
            // Insert or update user data
            $userID = $this->user->checkUser($userData);

            if(!empty($userID)){
                $this->view['userData'] = $userData;

            } else {
                $this->view['userData'] = array();
            }
        }
        //else {
       //   redirect(base_url());
       // }
        $this->session->sess_destroy();
       redirect(base_url().'start/gioco_quiz_wine_and_people/');


	}


	public function gioco_quiz_wine_and_people(){

        $session_data=$this->session->userdata();

        $domande_cat=$this->gestionedomande->get_domanda_categoria(1);
        $domande_cat=$domande_cat->result_array();

        $this->view['domande_cat']=$domande_cat[0];
        $risposte=$this->gestionerisposte->get_risposte_domanda($domande_cat[0]['id_domanda']);

        $risposte=$risposte->result_array();
        $this->view['risposte_domande']=$risposte;

        $this->view['title']='Letitwine | Vinitaly 2017';
        $this->load->view('domanda',$this->view);
    }

    public function gioco_quiz_wine_and_people2($id_domanda){



        if($this->input->post('risposta')) {

            $esito = $this->gestionerisposte->get_esito_risposta($this->input->post('risposta'));

            $this->session->set_userdata('domanda1',$esito);
            $this->session->set_userdata('id_domanda1',$id_domanda);
            $session_data=$this->session->userdata();

            $domande_cat = $this->gestionedomande->get_domanda_categoria2(1, $id_domanda);
            $domande_cat = $domande_cat->result_array();

            $this->view['domande_cat'] = $domande_cat[0];
            $risposte = $this->gestionerisposte->get_risposte_domanda($domande_cat[0]['id_domanda']);

            $risposte = $risposte->result_array();
            $this->view['risposte_domande'] = $risposte;

            $this->view['title'] = 'Letitwine | Vinitaly 2017';
            $this->load->view('domanda2', $this->view);
        }
        else{
            redirect(base_url());
        }
    }

    public function gioco_quiz_wine_itineraries($id_domanda){


        if($this->input->post('risposta')) {
            $esito = $this->gestionerisposte->get_esito_risposta($this->input->post('risposta'));
            $this->session->set_userdata('domanda2',$esito);
            $this->session->set_userdata('id_domanda2',$id_domanda);
            $session_data=$this->session->userdata();


            $domande_cat = $this->gestionedomande->get_domanda_categoria(2);
            $domande_cat = $domande_cat->result_array();

            $this->view['domande_cat'] = $domande_cat[0];
            $risposte = $this->gestionerisposte->get_risposte_domanda($domande_cat[0]['id_domanda']);

            $risposte = $risposte->result_array();
            $this->view['risposte_domande'] = $risposte;

            $this->view['title'] = 'Letitwine | Vinitaly 2017';
            $this->load->view('domanda3', $this->view);
        }
    }

    public function gioco_quiz_wine_itineraries2($id_domanda){


        if($this->input->post('risposta')) {
            $esito = $this->gestionerisposte->get_esito_risposta($this->input->post('risposta'));
            $this->session->set_userdata('domanda3',$esito);
            $this->session->set_userdata('id_domanda3',$id_domanda);
            $session_data=$this->session->userdata();

            $domande_cat = $this->gestionedomande->get_domanda_categoria2(2, $id_domanda);
            $domande_cat = $domande_cat->result_array();

            $this->view['domande_cat'] = $domande_cat[0];
            $risposte = $this->gestionerisposte->get_risposte_domanda($domande_cat[0]['id_domanda']);

            $risposte = $risposte->result_array();
            $this->view['risposte_domande'] = $risposte;

            $this->view['title'] = 'Letitwine | Vinitaly 2017';
            $this->load->view('domanda4', $this->view);
        }
    }

    public function gioco_quiz_style_food($id_domanda){

        if($this->input->post('risposta')) {
            $esito = $this->gestionerisposte->get_esito_risposta($this->input->post('risposta'));
            $this->session->set_userdata('domanda4',$esito);
            $this->session->set_userdata('id_domanda4',$id_domanda);
            $session_data=$this->session->userdata();

            $domande_cat = $this->gestionedomande->get_domanda_categoria(3);
            $domande_cat = $domande_cat->result_array();

            $this->view['domande_cat'] = $domande_cat[0];
            $risposte = $this->gestionerisposte->get_risposte_domanda($domande_cat[0]['id_domanda']);

            $risposte = $risposte->result_array();
            $this->view['risposte_domande'] = $risposte;

            $this->view['title'] = 'Letitwine | Vinitaly 2017';
            $this->load->view('domanda5', $this->view);
        }
    }

    public function gioco_quiz_style_food2($id_domanda){


        if($this->input->post('risposta')) {
            $esito = $this->gestionerisposte->get_esito_risposta($this->input->post('risposta'));
            $this->session->set_userdata('domanda5',$esito);
            $this->session->set_userdata('id_domanda5',$id_domanda);
            $session_data=$this->session->userdata();

            $domande_cat = $this->gestionedomande->get_domanda_categoria2(3, $id_domanda);
            $domande_cat = $domande_cat->result_array();

            $this->view['domande_cat'] = $domande_cat[0];
            $risposte = $this->gestionerisposte->get_risposte_domanda($domande_cat[0]['id_domanda']);

            $risposte = $risposte->result_array();
            $this->view['risposte_domande'] = $risposte;

            $this->view['title'] = 'Letitwine | Vinitaly 2017';
            $this->load->view('domanda6', $this->view);
        }
    }

    public function gioco_quiz_tips($id_domanda){

        if($this->input->post('risposta')) {
            $esito = $this->gestionerisposte->get_esito_risposta($this->input->post('risposta'));
            $this->session->set_userdata('domanda6', $esito);
            $this->session->set_userdata('id_domanda6',$id_domanda);
            $session_data = $this->session->userdata();

            $domande_cat = $this->gestionedomande->get_domanda_categoria(4);
            $domande_cat = $domande_cat->result_array();

            $this->view['domande_cat'] = $domande_cat[0];
            $risposte = $this->gestionerisposte->get_risposte_domanda($domande_cat[0]['id_domanda']);

            $risposte = $risposte->result_array();
            $this->view['risposte_domande'] = $risposte;

            $this->view['title'] = 'Letitwine | Vinitaly 2017';
            $this->load->view('domanda7', $this->view);
        }
    }

    public function gioco_quiz_tips2($id_domanda){


        if($this->input->post('risposta')) {
            $esito = $this->gestionerisposte->get_esito_risposta($this->input->post('risposta'));
            $this->session->set_userdata('domanda7',$esito);
            $this->session->set_userdata('id_domanda7',$id_domanda);
            $session_data=$this->session->userdata();

            $domande_cat = $this->gestionedomande->get_domanda_categoria2(4, $id_domanda);
            $domande_cat = $domande_cat->result_array();

            $this->view['domande_cat'] = $domande_cat[0];
            $risposte = $this->gestionerisposte->get_risposte_domanda($domande_cat[0]['id_domanda']);

            $risposte = $risposte->result_array();
            $this->view['risposte_domande'] = $risposte;

            $this->view['title'] = 'Letitwine | Vinitaly 2017';
            $this->load->view('domanda8', $this->view);
        }
    }

    public function calcolo_punteggio($id_domanda){


        if($this->input->post('risposta')) {
            $esito = $this->gestionerisposte->get_esito_risposta($this->input->post('risposta'));
            $this->session->set_userdata('domanda8',$esito);
            $this->session->set_userdata('id_domanda8',$id_domanda);
            $session_data=$this->session->userdata();
           // var_dump($session_data);
            $count=0;
            $domanda=1;
            $calcolo_punteggio=0;
            while($count<8){
                if($session_data['domanda'.$domanda]=='0' ) {
                    $calcolo_punteggio++;
                }
                $count++;
                $domanda++;

            }
           // echo $calcolo_punteggio.'<br />';
           // echo 'Domande capitate: '. '<br />';
            $count=1;
            while($count<=8){
               // echo $session_data['id_domanda'.$count]. '<br />';
                $count++;
            }

            $this->session->set_userdata('domande_corrette',$calcolo_punteggio);
            if($calcolo_punteggio>=0 && $calcolo_punteggio<=3){
               redirect(base_url().'astemio');
            }

            if($calcolo_punteggio>3 && $calcolo_punteggio<=6){
                redirect(base_url().'winelover');
            }

            if($calcolo_punteggio>6 && $calcolo_punteggio<=8){
                redirect(base_url().'sommelier');
            }



        }
    }


    public function risposteesatte($pagina){

        $session_data=$this->session->userdata();

        $risposte= array(

            'domanda1'=> $session_data['id_domanda1'],
             'domanda2'=> $session_data['id_domanda2'],
        'domanda3'=> $session_data['id_domanda3'],
        'domanda4'=> $session_data['id_domanda4'],
            'domanda5'=> $session_data['id_domanda5'],
            'domanda6'=> $session_data['id_domanda6'],
        'domanda7'=> $session_data['id_domanda7'],
        'domanda8'=> $session_data['id_domanda8']


        );

        $esatte= array(

            'esito1'=> $session_data['domanda1'],
            'esito2'=> $session_data['domanda2'],
            'esito3'=> $session_data['domanda3'],
            'esito4'=> $session_data['domanda4'],
            'esito5'=> $session_data['domanda5'],
            'esito6'=> $session_data['domanda6'],
            'esito7'=> $session_data['domanda7'],
            'esito8'=> $session_data['domanda8'],


        );


$this->view['url_ris']=$pagina;
        $this->view['domande']=$risposte;
        $this->view['risposte']=$esatte;
        $this->view['numero_esatte']=$session_data['domande_corrette'];

        $this->load->view('risposteesatteview', $this->view);

    }






}
