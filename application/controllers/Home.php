<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    protected $view;

    function __construct() {
        parent::__construct();

        $this->load->model('user');
    }

	public function index()
	{
        include_once APPPATH."libraries/facebook-api-php-codexworld/facebook.php";

        // Facebook API Configuration
        $appId = '1756418928007213';
        $appSecret = '041972df58adfb7179aac44c35cfecb7';
        $redirectUrl = base_url() . 'start';
        $fbPermissions = 'email';

        //Call Facebook API
        $facebook = new Facebook(array(
            'appId'  => $appId,
            'secret' => $appSecret

        ));

        $this->view['authUrl'] = $facebook->getLoginUrl(array('redirect_uri'=>$redirectUrl,'scope'=>$fbPermissions));

        $this->view['title']='Letitwine | Quiz Vinitaly 2017';
        $this->load->view('home',$this->view);


	}


}
